import "./App.css";
import ComingSoon from "./components/ComingSoon";
import Sidebar from "./components/Sidebar";
import IcBell from "./assets/icons/ic_bell.svg";

function App() {
  return (
    <div className="bg-gray-100 h-screen">
      <div className="layout w-screen flex h-screen p-8 space-x-8">
        <Sidebar />
        <div className="flex flex-col flex-1 h-full">
          <div className="flex justify-between items-center">
            <div>
              <h2 className="text-5xl font-bold mb-4">Welcome Bellrock!</h2>
              <p className="font-bold">Here is your dashboard </p>
            </div>
            <div className="flex items-center space-x-4">
              <img className="w-6 h-6" src={IcBell} alt='notification' />
              <img
                className="w-10 h-10 rounded-full"
                src="https://i.pravatar.cc/300"
                alt="Rounded avatar"
              />
            </div>
          </div>
          <div className="flex-1 rounded-2xl bg-white w-full mt-8">
            <div className="flex h-full w-full justify-center items-center">
              <ComingSoon />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
