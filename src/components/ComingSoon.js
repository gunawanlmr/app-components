import React from "react";
import IcComingSoon from "../assets/icons/ic_coming.svg";

export const ComingSoon = ({ image, title, description }) => {
  return (
    <div className="flex flex-col items-center">
      <img className="max-w-[32em] mb-4" src={image} alt={title} />
      <h2 className="font-bold md:text-2xl mb-2">{title}</h2>
      <p className="md:text-xl">{description}</p>
    </div>
  );
};

ComingSoon.defaultProps = {
  image: IcComingSoon,
  title: "Coming Soon",
  description: "We are making some changes",
};
export default ComingSoon;
