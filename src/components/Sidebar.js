import React, { useState } from "react";
import IcCalculator from "../assets/icons/ic_calculator.svg";
import IcDashboard from "../assets/icons/ic_dashboard.svg";
import IcCalendar from "../assets/icons/ic_calendar.svg";
import IcCertificate from "../assets/icons/ic_certificate.svg";
import IcDate from "../assets/icons/ic_date.svg";
import IcFinance from "../assets/icons/ic_finance.svg";
import IcGroup from "../assets/icons/ic_group.svg";
import IcTime from "../assets/icons/ic_time.svg";
import IcCaretUp from "../assets/icons/ic_caret_up.svg";
import IcCaretDown from "../assets/icons/ic_caret_down.svg";

const MENU_CONFIG = [
  { title: "Dashboard", active: true, icon: IcDashboard },
  { title: "Staff Management", icon: IcCertificate },
  { title: "Employee Management", icon: IcCertificate },
  { title: "Schedule", icon: IcDate },
  { title: "Timesheet", icon: IcTime },
  { title: "Reports", icon: IcCalendar },
  {
    title: "Organization",
    icon: IcGroup,
    items: [{ title: "Customer Sides" }, { title: "Employee" }],
  },
  { title: "Invoicing", icon: IcFinance },
  { title: "Financial", icon: IcCalculator },
];

export const Sidebar = () => {
  const [menus, setMenus] = useState(MENU_CONFIG);
  const handleToggleSubmenu = (index) => {
    setMenus((prev) => {
      const newMenus = [...prev];
      const menu = { ...prev[index] };
      menu.submenu = !menu.submenu;
      newMenus[index] = menu;
      return newMenus;
    });
  };

  const handleToggleActive = (index) => {
    setMenus((prev) =>
      prev.map((item, i) => ({ ...item, active: index === i }))
    );
  };

  return (
    <aside
      className="w-56 h-full shadow-sm border rounded-3xl bg-white h-full"
      aria-label="Sidebar"
    >
      {/* Heading */}
      <div className="flex flex-col items-center space-y-2 py-8 px-4">
        <img
          className="w-16 h-16 rounded-full"
          src="https://i.pravatar.cc/300"
          alt="Rounded avatar"
        />
        <h2 className="font-bold text-sm">Guard house</h2>
      </div>

      {/* Navigation */}
      <ul className="pt-4">
        {menus.map((item, i) => (
          <li
            onClick={() => handleToggleActive(i)}
            className={`p-4 ${
              item.active && "bg-[#F2385F] text-white rounded-md"
            }`}
            key={i}
          >
            <div className="flex items-center">
              <span className="block mr-4">
                <img className="w-6 h-6" src={item.icon} alt={item.title} />
              </span>
              <span
                className={`text-sm flex-1 text-left ${
                  item.active && "font-bold"
                }`}
              >
                {item.title}
              </span>
              {item.items && (
                <button
                  key={item.submenu}
                  className="cursor pointer block"
                  onClick={() => handleToggleSubmenu(i)}
                >
                  <img
                    className="w-4 h-4"
                    src={item.submenu ? IcCaretUp : IcCaretDown}
                    alt={item.title}
                  />
                </button>
              )}
            </div>

            {item.submenu && (
              <ul className="space-y-2 pl-8 mt-2">
                {item?.items?.map((submenu) => (
                  <li className="block flex px-2 py-1">{submenu.title}</li>
                ))}
              </ul>
            )}
          </li>
        ))}
      </ul>
    </aside>
  );
};

export default Sidebar;
